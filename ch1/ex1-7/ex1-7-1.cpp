/*************************************************************************
	> File Name: ex1-7-1.cpp
	> Author: Pansj
	> Mail: perry2008084@gmail.com 
	> Created Time: Sat 21 Dec 2013 01:20:10 AM CST
 ************************************************************************/

#include<iostream>
#include<fstream>
using namespace std;

string user_name = "perry";
int    num_tries = 0;
int    num_right = 0;
int  num = 50;

int main()
{
	ofstream outfile("seq_data.txt");
	if (!outfile)
		// for some reason, file can't open
		cerr << "Oops! Unable to save session data!\n";
	else
	{
		while (num--) {
		// ok: outfile open sucess, next to write the file
		outfile << user_name << ' '
				<< num_tries << ' '
				<< num_right << endl;
		}
	}	

	return 0;
}

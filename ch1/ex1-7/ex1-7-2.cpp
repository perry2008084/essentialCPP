/*************************************************************************
	> File Name: ex1-7-2.cpp
	> Author: Pansj
	> Mail: perry2008084@gmail.com 
	> Created Time: Sat 21 Dec 2013 01:30:13 AM CST
 ************************************************************************/

#include<iostream>
#include<fstream>
using namespace std;

int main()
{
	string name;
	int nc,nt;
	int line = 50;
	
	ifstream infile("seq_data.txt");

	while (line--) {
	if (!infile) {
		cerr << "read the file failure.\n";
	}
	else {
		while (infile >> name) {
			infile >> nt >> nc;	
			cout << name << endl;
		}
	}
	}
}

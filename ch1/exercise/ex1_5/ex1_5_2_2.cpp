/*************************************************************************
	> File Name: ex1_5_2_2.cpp
	> Author: Pansj
	> Mail: perry2008084@gmail.com 
	> Created Time: Sun 02 Mar 2014 05:20:41 PM CST
 ************************************************************************/

#include<iostream>
#include<string>
using namespace std;

int main()
{
	string user_name;

	cout << "Please enter your name: ";
	cin >> user_name;

	switch (user_name.size() ) {
		case 0:
			cout << "Ah, the user with no name. "
				 << "Well, ok, hi, user with no name.\n";
			break;
		case 1:
			cout << "A 1-character name? Hmm, have you  read Kafka?: "
				 << "hello, " << user_name << endl;
			break;
		default:
			cout << "Hello, " << user_name
				 << " -- happy to make your acquaintance!\n";
			break;
	}
		return 0;
}


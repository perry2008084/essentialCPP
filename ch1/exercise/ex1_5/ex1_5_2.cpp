/*************************************************************************
	> File Name: ex1_5_2.cpp
	> Author: Pansj
	> Mail: perry2008084@gmail.com 
	> Created Time: Sun Mar  2 11:08:50 2014
 ************************************************************************/

#include<iostream>
#include<string>
using namespace std;

int main()
{
	string str;
	bool isOK = false;

	do {
		cout << "Please input your name: "
			 << endl;
		cin >> str;
	} while( (str.size() <= 2)) ;

	cout << "Hello, " << str << endl;

	return 0;
}

/*************************************************************************
	> File Name: ex1_5_1.cpp
	> Author: Pansj
	> Mail: perry2008084@gmail.com 
	> Created Time: Sun Mar  2 10:44:19 2014
 ************************************************************************/

#include<iostream>
using namespace std;

int main()
{
	const int ARRAYSIZE = 100;
	char nameArray[ARRAYSIZE];
	bool isOk = false;
	int inputSize = 0;
	
	cout << "Please input your name: "
		 << endl;
	
	do {
		cin >> nameArray[inputSize++];
		
		if (inputSize < 2) {
			cout << "Please input more than 2 character."
				 << endl;
		}else {
			cout << "Hello, " << nameArray << endl; 
		}

	} while ((nameArray[inputSize] != '\n') && (inputSize < ARRAYSIZE));


	return 0;
}

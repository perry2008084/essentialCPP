/*************************************************************************
	> File Name: ex1-5-2.cpp
	> Author: Pansj
	> Mail: perry2008084@gmail.com 
	> Created Time: Sat 21 Dec 2013 03:03:00 PM CST
 ************************************************************************/

#include<iostream>
#include<vector>
using namespace std;

int main()
{
	const int seq_size = 18;
	int elem_vals[ seq_size ] = {
		1,2,3,3,4,7,2,5,12,
		3,6,10,4,9,16,5,12,22
	};

	vector<int> elem_seq( elem_vals, elem_vals+seq_size);

	cout << "The first " << elem_seq.size()
		 << " elements of the pell series:\n\t";

	for (int ix = 0; ix < elem_seq.size(); ++ix)
	{
		cout << elem_seq[ix] << ' ';
	}

	return 0;
}

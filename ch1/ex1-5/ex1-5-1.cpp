/*************************************************************************
	> File Name: ex1-5-1.cpp
	> Author: Pansj
	> Mail: perry2008084@gmail.com 
	> Created Time: Sat 21 Dec 2013 01:09:31 AM CST
 ************************************************************************/

#include<iostream>
#include<vector>
using namespace std;

int main()
{
	int seq_size = 18;
	int pell_seq[seq_size];
	vector<int> pell_seq_vec(seq_size);

	pell_seq[0] = 1;
	pell_seq[1] = 2;

	for (int ix = 2; ix < seq_size; ++ix)
	{
		pell_seq[ix] = pell_seq[ ix-2 ] + 2*pell_seq[ ix-1 ];
	}

	int ix = 0;
	while(seq_size--) {
		cout << pell_seq[ix++]
			 << ',';
	}

	return 0;
}

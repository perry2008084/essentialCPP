/*************************************************************************
	> File Name: conditionOperator.cpp
	> Author: Pansj
	> Mail: perry2008084@gmail.com 
	> Created Time: Thu 19 Dec 2013 08:44:00 PM CST
 ************************************************************************/

#include<iostream>
#include<string>
using namespace std;

int main()
{
	const int line_size = 8;
	const int line = 1;
	int cnt = 1;
	int index = 0;

	string a_string = "";

	// process the text to output
	for (int i = 0; i < line; i++) {
		cout << "please input a text: \n";
		// input the text
		cin >> a_string;
		
		cnt = a_string.size();
		// cnt = 8;  // for test

		// input text's length more than or equal 8
		while(cnt > line_size) {
			cout << a_string.substr(index, a_string.size() > line_size ? line_size : a_string.size() - index)
				 << (cnt % line_size ? ' ' : '\n');

			index += line_size;
			cnt -= line_size;
		}

		// input text's length less than 8
		cout << a_string;
	}

	return 0;
}

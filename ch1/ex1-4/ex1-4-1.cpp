/*************************************************************************
	> File Name: ex1-4-1.cpp
	> Author: pansj
	> Mail: perry2008084@gmail.com 
	> Created Time: Sat 21 Dec 2013 12:38:02 AM CST
 ************************************************************************/

#include<iostream>
#include<string>
using namespace std;

int main()
{
	bool next_seq = true;  // show the next sequence
	bool go_for_it = true;  // user want another try
	bool got_it = false;    // user got flag
	int num_tries = 0;		// user guess times
	int num_right = 0;		// user got the right answer times
	char user_rsp = ' ';
	const int next_elem = 5; // the next sequence

	while (next_seq == true)
	{
		// show the sequence to the user
		cout << "The sequence is: 1,1,2,3...\n";
		
		while ((got_it == false) && 
			  ( go_for_it == true ))
		{
			int user_guess;
			cin >> user_guess;
			num_tries++;
			if (user_guess == next_elem)
			{
				got_it = true;
				num_right++;
			}
			else
			{
				// user guess the wrong answer
				cout << "sorry,the answer is wrong.\n"
					 << "one more time to try? y\\n?\n";
				cin >> user_rsp;
				if ( user_rsp == 'N' || user_rsp == 'n')
				{
					go_for_it = false;
				}
			}
		}
		
		cout << "Want to try another sequence? (Y/N)";
		char try_again;
		cin >> try_again;

		if ( try_again == 'N' || try_again == 'n')
			next_seq = false;
	} // while(next_seq == true) end here
}

/*************************************************************************
	> File Name: ex1-2-1.cpp
	> Author: Pansj
	> Mail: perry2008084@gmail.com 
	> Created Time: Tue 17 Dec 2013 08:28:16 PM CST
 ************************************************************************/

#include<iostream>
#include<string>

using namespace std;

int main()
{

const int max_tries = 3;	// the max times for user to try

string user_name;			// user's name
int user_val;				// user's input value
int num_tries = 0;			// user tries times
int num_right(0);			// user got the right answer times
double user_score = 0.0;	// user's score
char user_more;

cout << "The value 2, 3 for two consecutive elements of a numerical sequence.\n"
	 << "What is the next value?\n";

cout << "Please input your name.\n";
cin >> user_name;

for (int i = 0; i < max_tries; i++){
	num_tries++;
	cout << "Please input your guess value.\n"; 
	cin >> user_val;
	if(5 == user_val) {
		cout << "congratuations,you are right.\n";	
		num_right++;
		break;
	}else {
		cout << "Try another sequence? Y/N? \n";
		cin >> user_more;

		if (('N' == user_more) || ('N' == user_more)) {
			continue;	
		}
	}
}

// scores
user_score = num_right/num_tries;
cout << "The score you got is "
	 << user_score*100
	 << '%'
	 << endl;
}

/*************************************************************************
	> File Name: ex1-6-1.cpp
	> Author: Pansj
	> Mail: perry2008084@gmail.com 
	> Created Time: Sat 21 Dec 2013 03:30:45 PM CST
 ************************************************************************/

#include<iostream>
#include<vector>
using namespace std;

int main()
{
	const int seq_size = 8;
	int elem_fib[] = {
		1,1,2,3,5,6,13,21
	};
	vector<int> fibonacci( elem_fib, elem_fib+seq_size);

	int elem_luc[] = {
		1,3,4,7,11,18,29,47
	};
	vector<int> lucas( elem_luc, elem_luc+seq_size);

	int elem_pel[] = {
		1,2,5,12,29,70,169,408
	};
	vector<int> pell( elem_pel, elem_pel+seq_size);

	int elem_tri[] = {
		1,3,6,10,15,21,28,36
	};
	vector<int> triangular( elem_tri, elem_tri+seq_size);

	int ele_squ[] = {
		1,4,9,16,25,36,49,64
	};
	vector<int> square( ele_squ, ele_squ+seq_size);

	int ele_pen[] = {
		1,5,12,22,35,51,70,92
	};
	vector<int> pentagonal( ele_pen, ele_pen+seq_size);

	const int seq_cnt = 6;
	vector<int> *seq_addrs[seq_cnt] = {
		&fibonacci, &lucas, &pell, &triangular, &square, &pentagonal
	};

	vector<int> *current_vec = 0;

	for (int ix = 0; ix < seq_cnt; ++ix)
	{
		current_vec = seq_addrs[ix];
		cout << *current_vec << ' ';
	}

	return 0;
	
}
